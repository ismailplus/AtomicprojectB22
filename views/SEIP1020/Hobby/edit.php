<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\Hobby\Hobby;
use App\Bitm\SEIP1020\Utility\Utility;

$hobby= new Hobby();
$hobby->prepare($_GET);
$singleItem=$hobby->edit();



//object(stdClass)#5 (3) {
//["id"]=>
//  string(2) "27"
//["hobby"]=>
//  string(23) "Playing Football,Coding"
//["deleted_at"]=>
//  NULL
//}


//
//$OneItem=implode(",",$singleItem);
//Utility::dd($OneItem);
//    if(array_key_exists("hobby",$singleItem)){
//        $OneItem=explode(",","hobby");
//    }

//Utility::dd($OneItem);


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit your hobby</h2>

    <form role="form" method="post" action="update.php">
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Gardening" <?php if(in_array("Gardening",$singleItem))echo "checked";?>>Gardening</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Playing Football" <?php if(in_array("Playing Football",$singleItem))echo "checked";?>>Playing Football</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Coding" <?php if(in_array("Coding",$singleItem))echo "checked"; ?> >Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Cricket" <?php if(in_array("Cricket",$singleItem))echo "checked";?>>Cricket</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name=hobby[] value="Swimming" <?php if(in_array("Swimming",$singleItem))echo "checked";?>>Swimming</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>

