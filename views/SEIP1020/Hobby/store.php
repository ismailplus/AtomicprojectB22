<?php
include_once('../../../vendor/autoload.php');

use App\Bitm\SEIP1020\Hobby\Hobby;
use App\Bitm\SEIP1020\Utility\Utility;

//Utility::dd($_POST['hobby']);

$selected_hobby= $_POST['hobby'];
$comm_separated=implode(",",$selected_hobby);
//echo $comm_separated;
$_POST['hobby']=$comm_separated;
//Utility::dd($_POST);

$hobby= new Hobby();
$hobby-> prepare($_POST);
$hobby->store();
