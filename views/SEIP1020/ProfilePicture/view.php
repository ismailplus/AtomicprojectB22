<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\ProfilePicture\ImageUploader;
use App\Bitm\SEIP1020\Utility\Utility;

$profile_picture= new ImageUploader();
$profile_picture->prepare($_GET);
$singleItem=$profile_picture->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $singleItem->name?></h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
        <li class="list-group-item">Name: <?php echo $singleItem->name?></li>
       <li class="list-group-item" >Image: <img src="../../../Resources/Images/<?php echo $singleItem->images ?>" alt="image" height="300px" width="250px" class="img-responsive"></li>

    </ul>
</div>

</body>
</html>