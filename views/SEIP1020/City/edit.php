<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\City\City;
use App\Bitm\SEIP1020\Utility\Utility;

$single_city= new City();
$single_city->prepare($_GET);
$singleItem=$single_city->view();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gender Example EDit</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit your City Info</h2>
    <form role="form" method="post" action="update.php">
        <label>Edit Your name:</label>
        <input type="hidden" name="id" id="title" value="<?php echo $singleItem->id?>">
        <input type="text" name="name" class="form-control" id="title" value="<?php echo $singleItem->name ?>"></br>
        <h3>Edit Your City</h3>
        <select class="selectpicker" name="city_name">
            <option>Dhaka</option>
            <option>Chittagong</option>
            <option>Khulna</option>
            <option>Rajshahi</option>
            <option>Barisal</option>
            <option>Rangpur</option>
            <option>Kumilla</option>
            <option>Sylhet</option>
        </select></br>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>


