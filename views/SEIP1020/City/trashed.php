<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\Bitm\SEIP1020\City\City;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

$city= new City();
$cityList=$city->trashed();
//Utility::d($cityList);



?>


<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>All Trashed List</h2>
    <form action="recovermultiple.php" method="post" id="multiple">
        <a href="index.php" class="btn btn-primary" role="button">See All List</a>
        <button type="submit"  class="btn btn-primary">Recover Selected</button>
        <button type="button"  class="btn btn-primary" id="multiple_delete">Delete Selected</button>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Check Item</th>
                    <th>#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>City Name</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($cityList as $city){
                    $sl++; ?>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $city->id?>"></td>
                    <td><?php echo $sl?></td>
                    <td><?php echo $city-> id?></td>
                    <td><?php echo $city->name?></td>
                    <td><?php echo $city->city_name?></td>
                    <td><a href="recover.php?id=<?php echo $city-> id ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $city->id?>" class="btn btn-danger" role="button" id="delete">Delete</a>
                    </td>

                </tr>
                <?php }?>


                </tbody>
            </table>
    </form>
</div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });

    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

</script>

</body>
</html>
