<?php
include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP1020\City\City;

$city= new City();
$city->prepare($_GET);
$city->recover();