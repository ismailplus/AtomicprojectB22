
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gender Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select your Gender</h2>
    <form role="form" method="post" action="store.php">
        <label>Enter Your name:</label>
        <input type="text" name="name" class="form-control" id="title" placeholder="Enter Name"></br>
        <h3>Gender:</h3>
        <label class="radio-inline"><input type="radio" name="gender" value="male">Male</label>
        <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
        <label class="radio-inline"><input type="radio" name="gender" value="Others">Others</label>

        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>


