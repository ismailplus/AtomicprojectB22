<?php
namespace App\Bitm\SEIP1020\Email;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;

class Email
{
    public $id = "";
    public $email = "";
    public $title = "";
    public $conn;
    public $deleted_at;


    public function prepare($data = "")
    {
        if (array_key_exists("email", $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists("title", $data)) {
            $this->title = $data['title'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        //echo  $this;

    }
    
    
    
    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }

    
    
    public function store()
    {
        $query = "INSERT INTO `atomicprojectb22`.`email` (`email`) VALUES ('".$this->email."')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    
    
    public function index()
    {
        $_allEmail = array();
        $query = "SELECT * FROM `email` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_object($result)) {
            $_allEmail[] = $row;
        }

        return $_allEmail;
    }

    
    public function view()
    {
        $query = "SELECT * FROM `email` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }

    
    public function update()
    {
        $query = "UPDATE `atomicprojectb22`.`email` SET `email` = '".$this->email."' WHERE `email`.`id` = ".$this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Success!</strong> Data has been updated  successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    
    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`email` WHERE `email`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>NOt Deleted!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        }
    }

    
    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`email` SET `deleted_at` =".$this->deleted_at." WHERE `email`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Trashed!</strong> Data has been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Not Trashed!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        }
    }

    
    public function trashed()
    {
        $_allEmail = array();
        $query = "SELECT * FROM `email` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allEmail[] = $row;
        }

        return $_allEmail;
    }

    
    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`email` SET `deleted_at` = NULL WHERE `email`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recovered!</strong> Data has been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recovered!</strong> Data has not been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        }
    }
  
    
    
    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs>0))) {
            $ids = implode(",",$IDs);
            $query = "UPDATE `atomicprojectb22`.`email` SET `deleted_at` = NULL WHERE `email`.`id` IN(".$ids.")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn,$query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Recovered!</strong> Selected Data has been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Recovered!</strong> Selected Data has not been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            }
        }
    }

    
    
    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs>0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`email` WHERE `email`.`id`  IN(".$ids.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has not been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            }
        }
    }


}