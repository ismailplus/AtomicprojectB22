<?php
namespace App\Bitm\SEIP1020\Gender;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;


class Gender
{
    public $id = "";
    public $name = "";
    public $gender = "";
    public $deleted_at;
    public $conn;


    public function prepare($data = "")
    {
        if (array_key_exists("gender", $data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
    }    //echo  $this;


        public
        function __construct()
        {
            $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
        }

        public
        function store()
        {
            $query="INSERT INTO `atomicprojectb22`.`gender` (`name`, `gender`) VALUES ('".$this->name."', '".$this->gender."');";
            //$query = "INSERT INTO `atomicprojectb22`.`gender` (`gender`) VALUES ('" . $this->gender . "')";
            //echo $query;

            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                echo "Error";
            }
        }

        public
        function index()
        {
            $_allList = array();
            $query = "SELECT * FROM `gender` WHERE `deleted_at` IS NULL";
            $result = mysqli_query($this->conn, $query);
            while ($row = mysqli_fetch_object($result)) {
                $_allList[] = $row;
            }

            return $_allList;


        }

        public function view()
        {
            $query = "SELECT * FROM `gender` WHERE `id`=" . $this->id;
            $result = mysqli_query($this->conn, $query);
            $row = mysqli_fetch_object($result);
            return $row;
        }

    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`gender` WHERE `gender`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }
    
    public function edit(){
        $query = "SELECT * FROM `gender` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }



    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`gender` SET `deleted_at` =" . $this->deleted_at . " WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Trashed!</strong> Data has been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Trashed!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allList = array();
        $query = "SELECT * FROM `gender` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allList[] = $row;
        }

        return $_allList;

    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recover!</strong> Data has been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recover!</strong> Data has not been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`gender` SET `deleted_at` = NULL WHERE `gender`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Recover!</strong> Selected Data has been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Recover!</strong> Selected Data has not been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`gender` WHERE `gender`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has not been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function update()
    {
        $query="UPDATE `gender` SET `name` = '".$this->name."', `gender` = '".$this->gender."' WHERE `gender`.`id` = $this->id";
        //$query = "UPDATE `atomicprojectb22`.`gender` SET `gender` = '".$this->gender."' WHERE `hobby`.`id` = " . $this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }



}