<?php
namespace App\Bitm\SEIP1020\ProfilePicture;
use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;



class ImageUploader
{
    public $id = "";
    public $name = "";
    public $image_name = "";
    public $conn;


    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }

    public function prepare($data = "")
    {
        if (array_key_exists("name", $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists("image", $data)) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }

        return $this;

    }

    public function store()
    {
        $query = "INSERT INTO `atomicprojectb22`.`profilepicture` (`name`, `images`) VALUES ('" . $this->name . "', '" . $this->image_name . "')";

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }

    }

    public function index()
    {
        $_allinfo = array();
        $query = "SELECT * FROM `atomicprojectb22`.`profilepicture`WHERE `deleted_at` IS NULL ";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allinfo[] = $row;
        }

        return $_allinfo;

    }

    public function view()
    {
        $query = "SELECT * FROM `atomicprojectb22`.`profilepicture` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }


    public function update()
    {
        if (!empty($this->image_name)) {
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `name` = '" . $this->name . "', `images` = '" . $this->image_name . "' WHERE `profilepicture`.`id` =" . $this->id;
        } else {
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `name` = '" . $this->name . "' WHERE `profilepicture`.`id` =" . $this->id;

        }

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Success!</strong> Data has been updated  successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }


    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`profilepicture`WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }


    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `deleted_at` =" . $this->deleted_at . " WHERE `profilepicture`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Trashed!</strong> Data has been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Trashed!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allinfo = array();
        $query = "SELECT * FROM `profilepicture` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allinfo[] = $row;
        }

        return $_allinfo;

    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `deleted_at` = NULL WHERE `profilepicture`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recoverd!</strong> Data has been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recoverd!</strong> Data has not been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }


    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`profilepicture` SET `deleted_at` = NULL WHERE `profilepicture`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                 <strong>Recovered!</strong> Selected Data has been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Recovered!</strong> Selected Data has not been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`profilepicture` WHERE `profilepicture`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has not been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            }

        }
    }

}


?>



