<?php
namespace App\Bitm\SEIP1020\Hobby;

use App\Bitm\SEIP1020\Utility\Utility;
use App\Bitm\SEIP1020\Message\Message;


class Hobby{
    public $id="";
    public $title="";
    public $hobbies;
    public $hobby;
    public $hobbyList=array();
    public $conn;

    
    
    public function prepare($data="")
    {
        if (array_key_exists("id", $data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists("hobby", $data)) {
            $this->hobby = $data['hobby'];
        }


        //echo  $this;
    }

 

    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22")
        or die("Database connection failed");
    }

    public function store()
    {
        $query = "INSERT INTO `atomicprojectb22`.`hobby` (`hobby`) VALUES ('".$this->hobby."')";
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }

    public function index()
    {
        $hobbyList = array();
        $query = "SELECT * FROM `hobby` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn,$query);
        while($row = mysqli_fetch_object($result)) {
            $hobbyList[] = $row;
        }

        return $hobbyList;
    }


    public function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`hobby` WHERE `hobby`.`id` = ".$this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has not been deleted successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function view()
    {
        $query = "SELECT * FROM `hobby` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_object($result);
        return $row;
    }
    public function edit(){
        $query = "SELECT * FROM `hobby` WHERE `id`=".$this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }



    public function trash()
    {
        $this->deleted_at = time();
        $query = "UPDATE `atomicprojectb22`.`hobby` SET `deleted_at` =" . $this->deleted_at . " WHERE `hobby`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Deleted!</strong> Data has not been trashed successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function trashed()
    {
        $_allList = array();
        $query = "SELECT * FROM `hobby` WHERE `deleted_at` IS NOT NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $_allList[] = $row;
        }

        return $_allList;

    }

    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`hobby` SET `deleted_at` = NULL WHERE `hobby`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recover!</strong> Data has been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        } else {
            Message::message("
            <div class=\"alert alert-info\">
            <strong>Recover!</strong> Data has not been recovered successfully.
            </div>");
            Utility::redirect("index.php");
        }

    }

    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`hobby` SET `deleted_at` = NULL WHERE `hobby`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Recover!</strong> Selected Data has been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Recover!</strong> Selected Data has not been recovered successfully.
                </div>");
                Utility::redirect("index.php");
            }

        }

    }

    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`hobby` WHERE `hobby`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            } else {
                Message::message("
                <div class=\"alert alert-info\">
                <strong>Deleted!</strong> Selected Data has not been deleted successfully.
                </div>");
                Utility::redirect("index.php");
            }

        }
    }

    public function update()
    {
        $query = "UPDATE `atomicprojectb22`.`hobby` SET `hobby` = '".$this->hobby."' WHERE `hobby`.`id` = " . $this->id;
        //echo $query;

        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::message("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::redirect("index.php");
        } else {
            echo "Error";
        }
    }



}